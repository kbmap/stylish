# Stylish

[Stylish](https://userstyles.org) is an awsome tool to customize the websites as
I expected. This repository hosts styles customed by me.

- [My Dark DuckDuckGo][ddg]
- [My Dark GitLab][gitlab]
- [My Dark Quora][quora]
- [My Dark V2EX][v2ex]

[ddg]: https://coy.gitlab.io/stylish/my_dark_duckduckgo.css
[gitlab]: https://coy.gitlab.io/stylish/my_dark_gitlab.css
[quora]: https://coy.gitlab.io/stylish/my_dark_quora.css
[v2ex]: https://coy.gitlab.io/stylish/my_dark_v2ex.css
